import React from 'react';
import { Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

import { UserCreate, UserList} from './users';
import { authProvider } from './authProvider';
import { PostList } from './provider';
import { Dashboard } from './Dashboard';
//import { dataProvider, dataProvider2 } from './dataProvider';
import { useEffect, useState } from 'react';
import { NotifyCreate } from './notify';
import { SusbcriptorList } from './subscriptor';


export const App = () => {

  const dataProvider = jsonServerProvider('http://providers.aldia.site:8080/api-providers/providers');
  //const dataProvider = jsonServerProvider('http://localhost:8080/api-providers/providers');
  const username = localStorage.getItem('username');
  const [userId, setUserId] = useState(null);
  
  useEffect(() => {
    // Realiza la solicitud a tu API para obtener la lista de usuarios
    //fetch('http://localhost:8080/api-providers/providers/all')
  fetch('http://providers.aldia.site:8080/api-providers/providers/all')
      .then(response => response.json())
      .then(data => {
        const user = data.find((user) => user.email === username);
        if (user) {
          setUserId(user.id);
        }
      })
      .catch(error => {
        console.error('Error al obtener el ID del proveedor', error);
      });
  }, [username]);

  return (

    <Admin dataProvider={dataProvider} authProvider={authProvider} dashboard={Dashboard}>
      <Resource name="all" list={UserList} create={UserCreate} options={{ label: "Proveedor" }} />
      <Resource name={userId + '/users'} list={SusbcriptorList} options={{ label: "Susbcriptores" }} />
      <Resource name="sendMassiveNotify" list={NotifyCreate} options={{ label: "Notificar" }} />
    </Admin>

  );
};





/*export const App = () => (
  <Admin dataProvider={dataProvider}>
    <Resource name="api-providers/providers/all" list={UserList} options={{label: 'Usuario'}}/>
    <Resource name="posts"  list={PostList} edit={PostEdit} create={PostCreate} options={{ label: 'Provider' } }  />
    <Resource name="notify" create={NotifyCreate}/>
  </Admin>
);*/