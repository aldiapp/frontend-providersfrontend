import { Card, CardContent, CardHeader } from '@mui/material';
import { useEffect, useState } from 'react';

export const Dashboard = () => {
  const username = localStorage.getItem('username');
  const [userName, setUserName] = useState(null);
  const [userId, setUserId] = useState(null);
  
  useEffect(() => {
    // Realiza la solicitud a tu API para obtener la lista de usuarios
    //fetch('http://localhost:8080/api-providers/providers/all')
    fetch('http://providers.aldia.site:8080/api-providers/providers/all')
      .then(response => response.json())
      .then(data => {
        const user = data.find((user) => user.email === username);

        if (user) {
          setUserId(user.id);
          setUserName(user.name);
            
        }
      })
      .catch(error => {
        console.error('Error al obtener el ID del usuario:', error);
      });
  }, [username]);

  return (
    <Card>
      <CardHeader title={`Bienvenido ${userName}`} />
      <CardContent>
        {userId ? (
          <p>El email asociado a tu nombre es: {username}</p>
        ) : (
          <p>No se encontró un usuario con el email {username}</p>
        )}
      </CardContent>
    </Card>
  );
};
