import { List, Datagrid, TextField, AutocompleteInput, ReferenceField, EditButton, Edit, SimpleForm, ReferenceInput, TextInput, Create } from "react-admin"


import { NotificationsActive } from '@mui/icons-material';
//import { CreateDialog } from '@react-admin/ra-form-layout';
import { Button } from 'react-admin';



import { useNavigate } from 'react-router-dom';


const PostBulkActionButtons = ({ selectedIds }: { selectedIds: string[] }) => {
  const navigate = useNavigate();
  const handleCreate = () => {
    const dataToSend = {
      ids: selectedIds,
     
    };
    
    navigate(`/notify/create`, { state: dataToSend });
  };
  return (
    <Button label="Notificar" startIcon={<NotificationsActive />} onClick={handleCreate} />
  );
};


const postFilters = [
  <TextInput source="q" label="Search" alwaysOn />,
  <ReferenceInput source="userId" reference="users">
    <AutocompleteInput optionText="name" />
  </ReferenceInput>
];

export const PostList = (props: any) => (

  <>

    <List filters={postFilters}>
      <Datagrid {...props} bulkActionButtons={<PostBulkActionButtons selectedIds={[]} />}>
        <TextField source="id" />
        <ReferenceField source="userId" reference="users" >
          <TextField source="name" />
        </ReferenceField>
        <TextField source="title" />

        <EditButton />
      </Datagrid>
    </List>

  </>

);

export const PostEdit = () => (
  <Edit>
    <SimpleForm>
      <TextInput source="id" disabled />
      <ReferenceInput source="userId" reference="users">
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <TextInput source="title" />
      <TextInput source="body" multiline rows={5} />
    </SimpleForm>
  </Edit>
);

export const PostCreate = () => (
  <Create >
    <SimpleForm>
      <ReferenceInput source="userId" reference="users">
        <AutocompleteInput optionText="name" />
      </ReferenceInput>

      <TextInput source="title" />
      <TextInput source="body" multiline rows={5} />
    </SimpleForm>
  </Create>
);

