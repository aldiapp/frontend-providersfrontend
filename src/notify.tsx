
import { Create, SimpleForm, TextInput } from 'react-admin';

import { useLocation } from 'react-router-dom';

export const NotifyCreate = () => {
  const location = useLocation();
  const { ids } = location.state;
  console.log('ids:', Object.values(ids));


  const handleSave = (values:any) => {
    const { ids, title, body } = values;
    if (!ids || !Array.isArray(ids)) {
      console.error('IDs no válidos');
      return;
    }
    // Recorre la matriz de IDs y crea un registro para cada ID
    ids.forEach((ids) => {
      const newRecord = {
        userId: ids,
        title: title,
        body: body,
      };
      
      
      // Envía la solicitud al backend para crear el registro
      // Puedes utilizar fetch, axios u otra librería para realizar la solicitud
      fetch('API_ENDPOINT', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newRecord),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log('Registro creado:', data);
          // Aquí puedes realizar cualquier acción adicional después de crear el registro
        })
        .catch((error) => {
          console.error('Error al crear el registro:', error);
          // Manejo de errores
        });
    });
  };
  
  return (


    <Create>
      <SimpleForm onSubmit={handleSave}>
        <TextInput source="title" label="Channel"/>
        <TextInput source="body" label="Message" multiline rows={5} />
      </SimpleForm>
    </Create>
  );
};
