import { List, Datagrid, TextField, EmailField } from "react-admin";

import { NotificationsActive } from '@mui/icons-material';
//import { CreateDialog } from '@react-admin/ra-form-layout';
import { Button } from 'react-admin';



import { useNavigate } from 'react-router-dom';


const PostBulkActionButtons = ({ selectedIds }: { selectedIds: string[] }) => {
  const navigate = useNavigate();
  const handleCreate = () => {
    const dataToSend = {
      ids: selectedIds,
     
    };
    
    navigate(`/notify/create`, { state: dataToSend });
  };
  return (
    <Button label="Notificar" startIcon={<NotificationsActive />} onClick={handleCreate} />
  );
};
export const SusbcriptorList = (props:any) => (
    
  <List>
    <Datagrid {...props} bulkActionButtons={<PostBulkActionButtons selectedIds={[]} />}>
      <TextField source="name" />
      <TextField source="lastname" />
      <TextField source="phone" />
      <EmailField source="email" />

    </Datagrid>
  </List>


);
