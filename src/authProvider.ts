import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_CHECK, AUTH_ERROR } from 'react-admin';


export const authProvider = (type: string, params: { username?: any; password?: any; status?: any; }) => {
  if (type === AUTH_LOGIN) {
    const { username, password } = params;

    // Realiza una solicitud HTTP a tu API de autenticación
    return fetch('http://providers.aldia.site:8080/api-providers/auth/login', {
    //return fetch('http://localhost:8080/api-providers/auth/login', {
      method: 'POST',
      body: JSON.stringify({ email:username, password }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      if (response.status < 200 || response.status >= 300) {
        throw new Error(response.statusText);
      }

      return response.json();
    })
    .then(data => {
      // Guarda el token en el almacenamiento local (localStorage)
      localStorage.setItem('token', data.token);
      //window.location.replace('/all');
      localStorage.setItem('username',username);
      setTimeout(function(){
        window.location.reload();
     }, 10); 
      
    })
    .catch(error => {
      // Maneja los errores de autenticación
      throw new Error('Error de inicio de sesión');
    });
  }

  if (type === AUTH_LOGOUT) {
    // Elimina el token del almacenamiento local (localStorage)
    localStorage.removeItem('token');
    localStorage.removeItem('username')
    
    return Promise.resolve();
  }

  if (type === AUTH_CHECK) {
    // Comprueba si el token existe en el almacenamiento local (localStorage)
    return localStorage.getItem('token') ? Promise.resolve() : Promise.reject();
  }

  if (type === AUTH_ERROR) {
    const { status } = params;

    if (status === 401 || status === 403) {
      // Redirige al usuario a la página de inicio de sesión en caso de error de autenticación
      localStorage.removeItem('token');
      return Promise.reject();
    }

    return Promise.resolve();
  }

  return Promise.resolve();
};

